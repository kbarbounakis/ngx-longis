import { ModuleWithProviders, NgModule } from '@angular/core';

@NgModule({
  imports: [
  ],
  declarations: [],
  exports: []
})
export class LongisSharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: LongisSharedModule,
      providers: []
    }
  }
}
