export const REGISTRAR_APP_LOCATIONS = [
    {
      'privilege': 'Location',
      'target': {
          'url': '^/auth/'
      },
      'mask': 1
    },
    {
      'privilege': 'Location',
      'target': {
          'url': '^/error'
      },
      'mask': 1
    },
    {
      'privilege': 'Location',
      'account': {
          'name': 'Administrators'
      },
      'target': {
          'url': '^/'
      },
      'mask': 1
    },
    {
      'privilege': 'Location',
      'account': {
        'name': 'Registrar'
      },
      'target': {
        'url': '^/departments/current/documents/series/\\d+/items/parent'
      },
      'mask': 0
    },
    {
      'privilege': 'Location',
      'account': {
        'name': 'Registrar'
      },
      'target': {
        'url': '^/candidate-students/list/*'
      },
      'mask': 1
    },
    {
      'privilege': 'Location',
      'account': {
        'name': 'Registrar'
      },
      'target': {
        'url': '^/candidate-students/upload-actions/list/*'
      },
      'mask': 0
    },
    {
      'privilege': 'Location',
      'account': {
        'name': 'Registrar'
      },
      'target': {
        'url': '^/settings/lists/CandidateSources/*'
      },
      'mask': 0
    },
    {
      'privilege': 'Location',
      'account': {
        'name': 'Registrar'
      },
      'target': {
        'url': '^/departments/current/restore/students/*'
      },
      'mask': 0
    },
    {
      'privilege': 'Location',
      'account': {
        'name': 'Registrar'
      },
      'target': {
        'url': '^/'
      },
      'mask': 1
    }
  ];
  