export const REGISTRAR_SIDEBAR_LOCATIONS = [
  {
    name: 'Sidebar.Instructors',
    key: 'Sidebar.Instructors',
    url: '/instructors/list/index',
    icon: 'fa fa-bullhorn',
    index: 0
  },
  {
    name: 'Sidebar.Students',
    key: 'Sidebar.Students',
    url: '/students',
    icon: 'fa fa-graduation-cap',
    index: 10
  },
  {
    name: 'Sidebar.Teaching',
    key: 'Sidebar.Teaching',
    url: 'javascript:void(0)',
    icon: 'fa fa-history',
    index: 20,
    children: [
      {
        name: 'Sidebar.Courses',
        key: 'Sidebar.Courses',
        url: '/courses',
        icon: 'fa fa-none',
        index: 0
      },
      {
        name: 'Sidebar.Classes',
        key: 'Sidebar.Classes',
        url: '/classes',
        icon: 'fa fa-none',
        index: 5
      },
      {
        name: 'Sidebar.Exams',
        key: 'Sidebar.Exams',
        url: '/exams',
        icon: 'fa fa-none',
        index: 10
      },
      {
        name: 'Sidebar.GradeSubmissions',
        key: 'Sidebar.GradeSubmissions',
        url: '/grade-submissions',
        icon: 'fa fa-none',
        index: 15
      }
    ]
  },
  {
    name: 'Sidebar.StudyPrograms',
    key: 'Sidebar.StudyPrograms',
    url: '/study-programs',
    icon: 'fa fa-university',
    index: 30
  },
  {
    name: 'Sidebar.Theses',
    key: 'Sidebar.Theses',
    url: '/theses',
    icon: 'fa fa-briefcase',
    index: 40
  },
  {
    name: 'Sidebar.Requests',
    key: 'Sidebar.Requests',
    url: '/requests',
    icon: 'fa fa-envelope',
    index: 50
  },
  {
    name: 'Sidebar.Events',
    key: 'Sidebar.Events',
    url: '/events',
    icon: 'fa fa-calendar',
    index: 60
  },
  {
    name: 'Sidebar.Admissions',
    key: 'Sidebar.Admissions',
    url: 'javascript:void(0)',
    icon: 'fa fa-address-book',
    index: 15,
    children: [
      {
        name: 'Sidebar.EnrollmentEvents',
        key: 'Sidebar.EnrollmentEvents',
        url: '/enrollment-events',
        icon: 'fa fa-none',
        index: 1
      },
      {
        name: 'Sidebar.Candidates',
        key: 'Sidebar.Candidates',
        url: '/candidate-students/list',
        icon: 'fa fa-none',
        index: 2
      },
      {
        name: 'Sidebar.Requests',
        key: 'Sidebar.Requests',
        url: '/candidates/list',
        icon: 'fa fa-none',
        index: 3
      },
      {
        name: 'Sidebar.CandidateStudentUploadActions',
        key: 'Sidebar.CandidateStudentUploadActions',
        url: '/candidate-students/upload-actions/list',
        icon: 'fa fa-none',
        index: 4
      },
      {
        name: 'Sidebar.CandidateSources',
        key: 'Sidebar.CandidateSources',
        url: '/settings/lists/CandidateSources',
        icon: 'fa fa-none',
        index: 5
      }
    ]
  },
  {
    name: 'Sidebar.More',
    key: 'Sidebar.More',
    url: 'javascript:void(0)',
    class: 'open',
    icon: 'fa fa-archive',
    index: 70,
    children: [
      {
        name: 'Sidebar.Statistics',
        key: 'Sidebar.Statistics',
        url: '/statistics',
        icon: 'fa fa-none',
        index: 20
      },
      {
        name: 'Sidebar.ArchivedDocuments',
        key: 'Sidebar.ArchivedDocuments',
        url: '/departments/current/documents',
        icon: 'fa fa-none',
        index: 35
      },
      {
        name: 'Sidebar.Settings',
        key: 'Sidebar.Settings',
        url: '/settings',
        icon: 'fa fa-settings',
        index: 40
      }
    ]
  },
  {
    name: 'LogoutLink',
    key: 'LogoutLink',
    url: '/auth/logout',
    icon: 'fa fa-lock',
    index: 0,
    'class': 'd-block d-md-none'
  }
];
